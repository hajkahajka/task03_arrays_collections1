package task_02;

import task_02.Door;

import java.util.Arrays;

public class Room {
    private Door[] doors;

    public Door[] getDoors() {
        return doors;
    }

    public void setDoors(Door[] doors) {
        this.doors = doors;
    }

    public Room() {
        doors = new Door [10] ;
        for (int i = 0; i < 10; i++) {
            doors[i] = new Door();
        }
    }

    public void printCloseDoors(){
        for (int i = 0; i < doors.length; i++) {
            if(doors[i].isOpen()==false){
                System.out.print(" "+ (i+1));
            }
        }
        System.out.println();
    }


    @Override
    public String toString() {
        return "Room{" +
                "doors=" + Arrays.toString(doors) +
                '}';
    }
}
