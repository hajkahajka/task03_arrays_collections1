package task_02;

import java.util.Scanner;

public class Player {
    private int strength;

    public Player() {
        strength = 25;
    }

    public void openDoor (Door door){
        strength+=door.getFind().getStrength();
        door.setOpen(true);
    }

    public void playerMove(Room room){
        int choice;
        do {
            System.out.print("\nChoose the door you want to open: ");
            room.printCloseDoors();
            System.out.println("(To use the hint, enter number 0)");
            Scanner in = new Scanner(System.in);
            choice = in.nextInt();
            if (choice==0){
                printNubmersToWin(room);
                System.out.print("\nChoose the door you want to open: ");
                room.printCloseDoors();
                choice = in.nextInt();
            }
            if (room.getDoors()[choice-1].isOpen()){
                System.out.println("Door has already opened.");
            }
        }while (room.getDoors()[choice-1].isOpen());
        openDoor(room.getDoors()[choice-1]);
    }

    public int getStrength() {
        return strength;
    }

    public void printNubmersToWin(Room room) {
        int strength = getStrength();
        System.out.println("Way to win)))");
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < room.getDoors().length; i++) {
            if (room.getDoors()[i].getFind().getStrength() > 0) {
                strength += room.getDoors()[i].getFind().getStrength();
                stringBuffer.append(i + "  ");
            }
        }
        for (int i = 0; i < room.getDoors().length; i++) {
            if (room.getDoors()[i].getFind().getStrength() < 0) {
                strength += room.getDoors()[i].getFind().getStrength();
                stringBuffer.append(i + "  ");
            }
        }
        if (strength >= 0) {
            System.out.println(stringBuffer.toString());
        } else {
            System.out.println("\nThat's a pity, but you can't win");
        }
    }

    @Override
    public String toString() {
        return "Player{" +
                "strength=" + strength +
                '}';
    }
}
