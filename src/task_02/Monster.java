package task_02;

public class Monster implements Find {
    private int strength;

    public Monster() {
        strength -= (int)(Math.random()*95)+5;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "\tMonster {" +
                "\tstrength=\t" + (-strength) +
                '}';
    }
}
