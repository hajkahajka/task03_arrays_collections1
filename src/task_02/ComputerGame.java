package task_02;

public class ComputerGame {
    private Player player;
    private Room room;

    public ComputerGame() {
        player = new Player();
        room = new Room();
    }

    public static void main(String[] args) {
        ComputerGame computerGame = new ComputerGame();
        System.out.println(computerGame);
        computerGame.startGame();
    }

    public void startGame() {
        for (int i = 0; i < room.getDoors().length; i++) {
            System.out.println("\nYour destiny will be there after you pass "
                    + countdownToDeath(room.getDoors(), 0) + " doors.");
            player.playerMove(room);
            System.out.println(player);
            if (!isAlive()) {
                System.out.println("\nYou Died");
                break;
            }
            if (i == 9) {
                System.out.println("\nYou Won");
            }
        }
    }

    public boolean isAlive() {
        if (player.getStrength() < 0) {
            return false;
        } else {
            return true;
        }
    }

    public int countdownToDeath(Door[] doors, int counter) {
        if (counter == 10) {
            return 0;
        }
        if (doors[counter].getFind().getStrength() + player.getStrength() < 0) {
            return countdownToDeath(doors, ++counter) + 1;
        } else {
            return countdownToDeath(doors, ++counter);
        }
    }

    @Override
    public String toString() {
        return "ComputerGame\n" +
                "player=" + player +
                ", \nroom=" + room +
                '}';
    }
}
