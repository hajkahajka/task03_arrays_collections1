package task_01;

import java.util.Arrays;
import java.util.Scanner;

public class ThreeArrays {
    public static final int ARRAY_SIZE = 20;
    private int[] firstArray;
    private int[] secondArray;
    private int[] thirdArray;

    public ThreeArrays() {
        this.firstArray = new int[ARRAY_SIZE];
        this.secondArray = new int[ARRAY_SIZE];
        this.thirdArray = new int[ARRAY_SIZE];
        for (int i = 0; i < ARRAY_SIZE; i++) {
            firstArray[i] = (int) (Math.random() * ((ARRAY_SIZE-1) / 2.0)+1);
            secondArray[i] = (int) (Math.random() * ((ARRAY_SIZE-1) / 2.0)+1);
        }
        System.out.print("First array: ");
        for (int i : firstArray) {
            System.out.print(i + "  ");
        }
        System.out.print("\nSecond array: ");
        for (int i : secondArray) {
            System.out.print(i + "  ");
        }
    }

    public static void main(String[] args) {
        ThreeArrays threeArrays = new ThreeArrays();
        threeArrays.createNewArray();
        System.out.println(threeArrays);
        threeArrays.deleteRepeat();
        System.out.println(threeArrays);
        threeArrays.removeDoumleRepeat();
        System.out.println(threeArrays);
    }

    public void createNewArray() {
        System.out.println("\nChose method:\nCreate array:\t1- which element appear in both arrays." +
                "\n\t\t\t\t2- which element appear only in one array.");
        int choice;
        int counter = 0;
        Scanner in = new Scanner(System.in);
        choice = in.nextInt();
        if (choice == 1) {
            for (int i = 0; i < firstArray.length; i++) {
                for (int j = 0; j < secondArray.length; j++) {
                    if (firstArray[i] == secondArray[j]) {
                        thirdArray[counter] = firstArray[i];
                        counter++;
                        break;
                    }
                }
            }
        } else {
            for (int i = 0; i < firstArray.length; i++) {
                for (int j = 0; j < secondArray.length; j++) {
                    if (firstArray[i] == secondArray[j]) {
                        break;
                    } else if (firstArray[i] != secondArray[j] && j == secondArray.length - 1) {
                        thirdArray[counter] = firstArray[i];
                        counter++;
                    }
                }
            }
        }
    }

    public void deleteRepeat() {
        System.out.println("Remove repeat. Chose array :\n\t1-first" +
                "\n\t2-second\n\t3-third");
        int choice;
        int[] copyArray;
        Scanner in = new Scanner(System.in);
        choice = in.nextInt();
        if (choice == 1) {
            copyArray = firstArray;
        } else {
            if (choice == 2) {
                copyArray = secondArray;
            } else {
                copyArray = thirdArray;
            }
        }

        int[] newArray = new int[ARRAY_SIZE];
        int counter = 0;
        for (int i = 0; i < copyArray.length - 1; i++) {
            if (copyArray[i] != copyArray[i + 1]) {
                newArray[counter] = copyArray[i];
                counter++;
            }
        }
        if (choice == 1) {
            firstArray = newArray;
        } else {
            if (choice == 2) {
                secondArray = newArray;
            } else {
                thirdArray = newArray;
            }
        }
    }

    public void removeDoumleRepeat() {
        System.out.println("Remove double repeat. Chose array :\n\t1-first" +
                "\n\t2-second\n\t3-third");
        int choice;
        int[] copyArray;
        Scanner in = new Scanner(System.in);
        choice = in.nextInt();
        if (choice == 1) {
            copyArray = firstArray;
        } else {
            if (choice == 2) {
                copyArray = secondArray;
            } else {
                copyArray = thirdArray;
            }
        }
        int[] newArray = new int[ARRAY_SIZE];
        int counter = 0;
        int countRepeat=0;
        for (int i = 0; i < copyArray.length-1; i++) {
            for (int j = i+1; j < copyArray.length; j++) {
                if (copyArray[i]==copyArray[j]){
                    if(countRepeat>2) {
                        break;
                    }
                    countRepeat++;
                }else {
                    if(j==copyArray.length-1){
                        newArray[counter]=copyArray[i];
                        counter++;
                    }
                }
            }
        }
        if (choice == 1) {
            firstArray = newArray;
        } else {
            if (choice == 2) {
                secondArray = newArray;
            } else {
                thirdArray = newArray;
            }
        }
    }

    @Override
    public String toString() {
        return "ThreeArrays{" +
                "\nfirstArray=" + Arrays.toString(firstArray) +
                ",\nsecondArray=" + Arrays.toString(secondArray) +
                ",\nthirdArray=" + Arrays.toString(thirdArray) +
                '}';
    }
}
